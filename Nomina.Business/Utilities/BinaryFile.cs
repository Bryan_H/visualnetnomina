﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nomina.Business.Utilities
{
    class BinaryFile
    {
        public static BinaryReader hraf;
        public static int SIZE;

        public static int runBinarySearchRecursively(int key, int low, int high)
        {
        if(hraf == null){
            return -1;
        }
        int middle = (low + high) / 2;

        if (high<low) {
            return -1;
        }
        long pos = 8 + SIZE * middle;
        hraf.BaseStream.Seek(0, SeekOrigin.Begin);
        int id = hraf.ReadInt32();
        if (key == id) {
            return middle;
        } else if (key<id) {
            return runBinarySearchRecursively(
                    key, low, middle - 1);
        } else {
            return runBinarySearchRecursively(
                    key, middle + 1, high);
        }
      }
    }
}
