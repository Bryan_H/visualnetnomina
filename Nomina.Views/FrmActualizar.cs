﻿using Nomina.Business.Entities;
using Nomina.Business.Implements;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nomina.Views
{
    public partial class FrmActualizar : Form
    {
        public List<Empleado> empleados;
        public int Row { get; set; }

        private DaoEmpleadoImplements daoEmpleado;

        public FrmActualizar()
        {
            empleados = new List<Empleado>();
            daoEmpleado = new DaoEmpleadoImplements();
            InitializeComponent();
        }

        private void BtnAceptar_Click(object sender, EventArgs e)
        {
            validateForm();

            Empleado empleado = new Empleado();

            empleado.Id = Int32.Parse(txtId.Text);
            empleado.Nombres = txtBNombres.Text;
            empleado.Apellidos = txtApellidos.Text;
            empleado.Cedula = mtxtCedula.Text;
            empleado.Telefono = mtxtTelefono.Text;
            empleado.Salario = Single.Parse(txtSalario.Text);
            empleado.Direccion = txtDireccion.Text;
            empleado.FechaContratacion = dtpFecha.Value;

            daoEmpleado.Update(empleado);
            Dispose();
        }

        private void FrmActualizar_Load(object sender, EventArgs e)
        {
            load(Row);
        }

        private void load(int row)
        {
            empleados = daoEmpleado.All();
            Empleado empleado;
            empleado = empleados.ElementAt(row);

            txtId.Text = empleado.Id.ToString();
            txtBNombres.Text = empleado.Nombres;
            txtApellidos.Text = empleado.Apellidos;
            mtxtCedula.Text = empleado.Cedula;
            mtxtTelefono.Text = empleado.Telefono;
            txtDireccion.Text = empleado.Direccion;
            txtSalario.Text = empleado.Salario.ToString();
            dtpFecha.Value = new DateTime(empleado.FechaContratacion.Year,
                empleado.FechaContratacion.Month, empleado.FechaContratacion.Day);

        }

        private void validateForm()
        {
            if (txtBNombres.Text == "" && txtApellidos.Text == "" && 
                mtxtCedula.Text == "" && txtSalario.Text == "")
            {
                MessageBox.Show("No puede dejar vacios estos campos", "Campos inválidos");
            }
        }

        private void TxtBNombres_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validar.letras(e);
        }

        private void TxtTelefono_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validar.numeros(e);
        }

        private void TxtSalario_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validar.numerosDecimales(e, txtSalario);
        }

        private void TxtCedula_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Validar.cedula(e, txtCedula, txtBNombres);
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            Dispose();
        }
    }
}
